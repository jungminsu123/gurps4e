## GURPS4e Issues
Identify follow-up action that needs to take place as a result of the research observation or data, and a clear recommendation or action associated with it. An actionable insight both defines the insight and clearly calls out the next step. These insights are tracked over time.

#### Link

- [ ] Provide the link to any Dovetail or Fork you created (this should contain all the essential details, proposed and attempted solutions)
- [ ] Clearly Identify and provide links to other projects or applications that achieve similiar functionality to the proposed.

#### Assign

- [ ] MAINTERS ONLY: Assign this issue to the appropriate Product Manager, Product Designer, or UX Researcher

#### Description

- [ ] Provide detials on the ISSUE and the action to take

-------------------------------------------------------------------------------

|   |  PLEASE COMPLETE THE BELOW  |
| ------ | ------ |
| Dovetail link: | (URL goes here) |
| Issue Details: | (details go here) |
| Action to take: | (action goes here) |







 ~"GURPS4e Issues" 
